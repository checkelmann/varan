# Varan

Varan is a small CLI utility to export GitLab project level CI/CD Variables or import them using a JSON file.

## Note

This is a first draft, it will delete all of your existing variables and creating new variables configured in the JSON file.

## Usage

```
# varan --help
With Varan you are able to manage GitLab CI/CD Variables
with a configuration file which could be inside of a Git-Repository,
to have a versioned history of your CI/CD Settings.

Usage:
  varan [command]

Available Commands:
  export      Export CI/CD Variables
  help        Help about any command
  import      Import CI/CD Variables

# varan export --help
Export CI/CD Variables to a JSON file from an existing Project

Usage:
  varan export [flags]

Flags:
  -f, --filename string                Output file name (default "vars.json")
  -g, --gitlab-url string              GitLab Instance URL (default "https://gitlab.com")
  -h, --help                           help for export
  -o, --output string                  Output format json|table|file (default "table")
  -p, --personal-access-token string   GitLab API Personal Access Token
  -i, --project-id string              GitLab Project ID

# varan import --help
Import CI/CD Variables from a given JSON file to a GitLab Project

Usage:
  varan import [flags]

Flags:
  -f, --file string                    Import file name (default "vars.json")
  -g, --gitlab-url string              GitLab Instance URL (default "https://gitlab.com")
  -h, --help                           help for import
  -p, --personal-access-token string   GitLab API Personal Access Token
  -i, --project-id string              GitLab Project ID
```