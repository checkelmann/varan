FROM golang:1.15
WORKDIR /go/src/gitlab.com/checkelmann/varan/
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o varan .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/gitlab.com/checkelmann/varan/varan /usr/bin/varan
RUN chmod +x /usr/bin/varan
CMD ["/bin/sh"]
