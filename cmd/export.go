/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

type exportSettingsStruct struct {
	pat            *string
	api            *string
	id             *string
	outputFormat   *string
	outputFileName *string
}

var exportSettings exportSettingsStruct

// exportCmd represents the export command
var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "Export CI/CD Variables",
	Long:  `Export CI/CD Variables to a JSON file from an existing Project`,
	Run: func(cmd *cobra.Command, args []string) {
		git, err := gitlab.NewClient(*exportSettings.pat, gitlab.WithBaseURL(*exportSettings.api+"/api/v4"))
		if err != nil {
			log.Fatalf("Failed to create client: %v", err)
		}

		opt := &gitlab.ListProjectVariablesOptions{
			PerPage: 1000,
			Page: 1,
		}

		variables, _, err := git.ProjectVariables.ListVariables(*exportSettings.id, opt)
		jsonBytes, err := json.MarshalIndent(variables, "", "   ")
		if err != nil {
			fmt.Println(err.Error())
		}
		if *exportSettings.outputFormat == "table" {
			w := new(tabwriter.Writer)
			w.Init(os.Stdout, 10, 8, 2, '\t', 0)
			fmt.Fprintln(w, "KEY\tVALUE\tVARIABLE_TYPE\tPROTECTED\tMASKED\tENVIRONMENT_SCOPE")
			for _, variable := range variables {
				fmt.Fprintln(w, variable.Key+"\t"+variable.Value+"\t"+string(variable.VariableType)+"\t"+strconv.FormatBool(variable.Protected)+"\t"+strconv.FormatBool(variable.Masked)+"\t"+variable.EnvironmentScope)
			}
			w.Flush()

		} else if *exportSettings.outputFormat == "file" {

			_ = ioutil.WriteFile(*exportSettings.outputFileName, jsonBytes, 0644)
		} else {
			fmt.Println(string(jsonBytes))
		}
	},
	Args: func(cmd *cobra.Command, args []string) error {
		return nil
	},
}

func init() {
	rootCmd.AddCommand(exportCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	exportSettings.pat = exportCmd.PersistentFlags().StringP("personal-access-token", "p", "", "GitLab API Personal Access Token")
	exportSettings.api = exportCmd.PersistentFlags().StringP("gitlab-url", "g", "https://gitlab.com", "GitLab Instance URL")
	exportSettings.id = exportCmd.PersistentFlags().StringP("project-id", "i", "", "GitLab Project ID")
	exportSettings.outputFormat = exportCmd.PersistentFlags().StringP("output", "o", "table", "Output format json|table|file")
	exportSettings.outputFileName = exportCmd.PersistentFlags().StringP("filename", "f", "vars.json", "Output file name")
}
