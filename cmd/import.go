/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

type importSettingsStruct struct {
	pat                    *string
	api                    *string
	id                     *string
	file                   *string
	updateEnvironmentScope *bool
}

var importSettings importSettingsStruct

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Import CI/CD Variables",
	Long:  `Import CI/CD Variables from a given JSON file to a GitLab Project`,
	Run: func(cmd *cobra.Command, args []string) {

		git, err := gitlab.NewClient(*importSettings.pat, gitlab.WithBaseURL(*importSettings.api+"/api/v4"))
		if err != nil {
			log.Fatalf("Failed to create client: %v", err)
		}
		//variables, _, err := git.ProjectVariables.ListVariables(*exportSettings.id, &gitlab.ListProjectVariablesOptions{})
		var variables []*gitlab.ProjectVariable

		byteValue, err := ioutil.ReadFile(*importSettings.file)
		if err != nil {
			fmt.Println(err)
		}
		json.Unmarshal([]byte(byteValue), &variables)
		existingProjectVariables, _, err := git.ProjectVariables.ListVariables(*importSettings.id, &gitlab.ListProjectVariablesOptions{
			Page:    1,
			PerPage: 1000,
		})
		if err != nil {
			fmt.Println(err)
		}

		// Delete all existing Variables
		for _, existingVariable := range existingProjectVariables {
			fmt.Println(existingVariable.Key)
			fmt.Println(existingVariable.EnvironmentScope)
			err := RemoveVariableWithScope(*importSettings.id, existingVariable.Key, existingVariable.EnvironmentScope, *importSettings.api, *importSettings.pat)
			if err != nil {
				fmt.Println(err)
			}
		}

		for _, variable := range variables {

			// Create a new variable
			v := &gitlab.CreateProjectVariableOptions{
				Key:              gitlab.String(variable.Key),
				Value:            gitlab.String(variable.Value),
				VariableType:     gitlab.VariableType(variable.VariableType),
				Protected:        gitlab.Bool(variable.Protected),
				Masked:           gitlab.Bool(variable.Masked),
				EnvironmentScope: gitlab.String(variable.EnvironmentScope),
			}

			newprojectvar, _, err := git.ProjectVariables.CreateVariable(*importSettings.id, v)
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println(newprojectvar)

		}
	},
}

//RemoveVariableWithScope delete GitLab Variables with a scope
func RemoveVariableWithScope(pid string, key string, scope string, url string, token string) error {
	// curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/variables/VARIABLE_1"
	// curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/variables/VARIABLE_1?filter[environment_scope]=production"

	var apiurl string
	if scope != "*" {
		apiurl = url + "/api/v4/projects/" + pid + "/variables/" + key + "?filter[environment_scope]=" + scope
	} else {
		apiurl = url + "/api/v4/projects/" + pid + "/variables/" + key
	}
	req, err := http.NewRequest(http.MethodDelete, apiurl, nil)
	req.Header.Add("PRIVATE-TOKEN", token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
		return err
	}

	defer resp.Body.Close()
	return nil
}

func init() {
	rootCmd.AddCommand(importCmd)

	importSettings.pat = importCmd.PersistentFlags().StringP("personal-access-token", "p", "", "GitLab API Personal Access Token")
	importSettings.api = importCmd.PersistentFlags().StringP("gitlab-url", "g", "https://gitlab.com", "GitLab Instance URL")
	importSettings.id = importCmd.PersistentFlags().StringP("project-id", "i", "", "GitLab Project ID")
	importSettings.file = importCmd.PersistentFlags().StringP("file", "f", "vars.json", "Import file name")

}
